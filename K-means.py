import cv2
import numpy as np

image = cv2.imread('C:/Users/Omar-PC/Desktop/test.jpg')
image = cv2.resize(image, (600, 400))
Z = image.reshape(-1,3)

Z = np.float32(Z)


criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)

#no. of colors
k = 256

#intialize centers as random
flags = cv2.KMEANS_RANDOM_CENTERS

#run K-means
dist, label, LUT = cv2.kmeans(Z, k, None, criteria, 10, flags)

LUT = np.uint8(LUT)
res = LUT[label.flatten()]
res = res.reshape(image.shape)


cv2.imshow('Original', image)
cv2.imwrite('D:/usr/Documents/Uni/Pattern_Recognition/Original.jpg', image)
cv2.imwrite('D:/usr/Documents/Uni/Pattern_Recognition/K-means.bmp', res)
cv2.imshow('K = 256', res)

cv2.waitKey(0)
cv2.destroyAllWindows()