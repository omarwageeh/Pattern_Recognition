import cv2

image = cv2.imread('C:/Users/Omar-PC/Desktop/test.jpg')
resized = cv2.resize(image, (600, 400))

#convert to Gray
gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)

#Convert Gray image to Binary
(thresh, im_bw) = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

cv2.imshow('color_image', resized)
cv2.imwrite('D:/usr/Documents/Uni/Pattern_Recognition/gray.jpg', gray)
cv2.imshow('gray_image', gray)
cv2.imwrite('D:/usr/Documents/Uni/Pattern_Recognition/Binary.jpg', im_bw)
cv2.imshow('Binary', im_bw)
